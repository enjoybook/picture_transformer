$:.push File.expand_path('../lib', __FILE__)

# Maintain your gem's version:
require 'picture_transformer/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'picture_transformer'
  s.version     = PictureTransformer::VERSION
  s.authors     = ['EnjoybookTeam']
  s.email       = ['enjoybook.ru@gmail.com']
  s.homepage    = 'http://mynamebook.ru'
  s.summary     = 'Picture transformer for mynamebook'
  s.description = 'Picture transformer for mynamebook'
  s.license     = 'MIT'

  s.add_development_dependency 'rspec', '~> 3.0'
  s.add_development_dependency 'factory_bot'
  s.add_development_dependency 'byebug'
  s.add_development_dependency 'dotenv'
  s.add_development_dependency 'colorize'

  s.add_dependency 'tinify'
  s.add_dependency 'activesupport'
  s.add_dependency 'rmagick'
  s.add_dependency 'aws-sdk', '~> 2'
  s.add_dependency 'rake'
end
