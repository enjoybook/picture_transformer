require 'picture_transformer/version'
require 'picture_transformer/configuration'

require 'picture_transformer/services/config_loader'
require 'picture_transformer/services/s4ync'
require 'picture_transformer/services/remote_sync'

require 'picture_transformer/transformer'

require 'picture_transformer/strategies/namebook/assets'
require 'picture_transformer/strategies/namebook/print'

require 'picture_transformer/strategies/familybook/assets'
require 'picture_transformer/strategies/familybook/print'

require 'picture_transformer/strategies/newyearbook/assets'
require 'picture_transformer/strategies/newyearbook/print'

require 'picture_transformer/strategies/herobook/assets'
require 'picture_transformer/strategies/herobook/print'

require 'picture_transformer/strategies/professionbook/assets'
require 'picture_transformer/strategies/professionbook/print'

require 'picture_transformer/strategies/personbook/assets'
require 'picture_transformer/strategies/personbook/print'

require 'picture_transformer/services/checkers/familybook'
require 'picture_transformer/services/config_preprocessors/familybook'

module PictureTransformer

  def self.logger(*args)
    puts(*args)
  end

  def self.root
    File.expand_path '../..', __FILE__
  end

end
