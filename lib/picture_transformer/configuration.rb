module PictureTransformer
  class Configuration

    attr_accessor :tinify_key, :base_path, :output_path

    def initialize
      @tinify_key = ENV.fetch('TINIFY_KEY', '')
      @base_path = ENV.fetch('TRANSFORMER_BASE_PATH', '')
      @output_path = ENV.fetch('TRANSFORMER_OUTPUT_PATH', '')
    end

  end

  def self.configuration
    @configuration ||= Configuration.new
  end

  def self.configuration=(config)
    @configuration = config
  end

  def self.configure
    yield configuration
  end
end
