require 'active_support/core_ext/object/try'
require 'digest/md5'
require 'aws-sdk'

module PictureTransformer
  module Services
    class S4ync

      attr_accessor :muted

      def initialize(credentials, region, muted: false)
        @muted = muted
        aws_credentials = Aws::Credentials.new(credentials[:key_id], credentials[:access_key])
        ::Aws.config.update(credentials: aws_credentials, region: region)
      end

      def sync_images(dir, dist)
        bucket_name, bucket_dir = dist.split(':')
        bucket = get_bucket(bucket_name)
        dir_files = Dir.glob("#{dir}/**/*.jpg")
        files_count = dir_files.length

        STDOUT.sync = true
        dir_files.each_with_index do |file, index|
          log("#{index + 1} / #{files_count} ")
          dist_file = File.join(bucket_dir, file.gsub("#{dir}/", ''))
          smart_upload(bucket, file, dist_file)
        end
        STDOUT.sync = false
      end

      private

      def smart_upload(bucket, file, remote_file)
        return unless ::File.file?(file)

        bucket_file = get_remote_if_exist(bucket, remote_file)
        bucket_file_etag = bucket_file.try(:etag) rescue nil # Aws::S3::Errors::NotFound

        if bucket_file && bucket_file_etag == file_etag(file)
          log(". #{bucket.name} #{remote_file}")
        else
          log("U #{bucket.name} #{remote_file}")
          upload_file(bucket, remote_file, file)
        end
      end

      def get_remote_if_exist(bucket, filename)
        bucket.object(filename)
      rescue
        nil
      end

      def file_etag(file)
        "\"#{Digest::MD5.hexdigest(File.read(file))}\""
      end

      def upload_file(bucket, remote_file, file)
        bucket.put_object(
          acl: 'public-read',
          key: remote_file,
          body: open(file)
        )
      end

      def get_bucket(name)
        s3 = ::Aws::S3::Resource.new
        s3.bucket(name)
      end

      def log(*args)
        puts(*args) unless muted
      end

    end
  end
end
