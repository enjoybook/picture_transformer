require 'active_support/core_ext/hash/indifferent_access'
require 'yaml'

module PictureTransformer
  module Services
    class ConfigLoader

      def call(path)
        ::YAML.load_file(path)
      end

      def extend_config_with_chars(config)
        chars = ('а'..'я').to_a + ['ё']

        config.each_with_object([]) do |sheet, acc|
          if sheet['path'].include? '?'
            chars.each do |char|
              acc << replace_to_char(sheet, char)
            end
          else
            acc << sheet
          end
        end
      end

      def replace_to_char(sheet, char)
        result = sheet.clone
        result['char'] = char
        result['path'] = sheet['path'].gsub '?', char
        result
      end

    end
  end
end
