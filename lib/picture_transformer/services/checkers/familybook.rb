require 'pp'
require 'colorize'

module PictureTransformer
  module Services
    module Checkers

      class Familybook

        def initialize(path, images, options={})
          raise  ArgumentError.new('The "path" param is not specified') unless path
          @path = path
          @path_images = read_images
          @config_images = process_images(images, options)
        end

        def call
          check_path
        end

        private

        def check_path
          diff = differences

          if diff[:count].zero?
            puts "Your path \"#{@path}\" was checked. Everything is fine!".colorize(:green)
          else
            diff[:images].each { |image| puts image.colorize(:red) }
            puts "\nTotal: #{ diff[:count]}".colorize(:red)
          end
        end

        def differences
          union = @path_images | @config_images
          missing_files = union - @path_images
          extra_existing_files = union - @config_images

          images = missing_files + extra_existing_files

          {
            count: images.size,
            images: images,
          }
        end

        def process_images(images, options)
          return images.map { |image| image['path'] } unless options[:check_sliced]

          images.each_with_object([]) do |image, acc|
            path_without_extension = image['path'].split('.jpg').join
            acc.push("#{path_without_extension}_1.jpg")
            acc.push("#{path_without_extension}_2.jpg")
          end
        end

        def read_images
          images = Dir["#{@path}/**/*.jpg"]
          raise "Directory \"#{@path}\" is empty or does not exist" if images.empty?
          images.map { |image| image.sub("#{@path}/", '') }
        end

      end

    end
  end
end
