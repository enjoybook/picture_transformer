# frozen_string_literal: true

module PictureTransformer
  module Services
    class RemoteSync

      attr_accessor :muted

      def initialize(muted: false)
        @muted = muted
      end

      # @param [Object] dir    Source directory that will be uploaded
      # @param [Object] dest   Remote directory where source should be uploaded
      # @param [Object] server Remote server in 'user@host' format
      def sync_images(dir, dest, server)
        abs_dir = File.expand_path dir
        log("Syncing '#{abs_dir}'")
        log("    to the '#{dest}' at remote server")

        # --rsync-path="mkdir -p .." is needed to avoid rsync failure with creating nesting directories for target
        mkdir_fallback = "--rsync-path=\"mkdir -pv #{dest} && rsync\""

        args = [
          '-lptv',
          '--recursive', # Upload all subfolder's content
          '--checksum', # Compare files by checksum
          '--iconv=utf-8', # For ё and й
          # '--dry-run', # For debug
          '--progress' # Show uploading progress
        ].join(' ')

        command = "rsync #{args} #{mkdir_fallback} \"#{abs_dir}\" \"#{server}:#{dest}\""
        log("command: \n #{command}")

        system(command)
      end

      private

      def log(*args)
        puts(*args) unless muted
      end

    end
  end
end
