module PictureTransformer
  module Services
    module ConfigPreprocessor

      class Familybook

        def initialize(config)
          @config = config
        end

        def call
          processed_config
        end

        private

        def processed_config
          @config.flat_map { |row| process_row(row) }
        end

        def process_row(row)
          replaces = row['replaces']
          return row unless replaces
          combinations(replaces).map { |elements| image_path(row['path'], elements) }
        end

        def combinations(replaces)
          elements = replaces.map { |replace| processed_replace_elements(replace) }
          generate_combinations(elements)
        end

        def processed_replace_elements(replace)
          keep = replace['keep'] || []
          places = replace['places']
          elements = replace['elements']
          elements.combination(places).to_a.map { |el| stringify(el + keep) }
        end

        def generate_combinations(elements)
          [1].product(*elements).map { |r| r.slice(1..-1) }
        end

        def stringify(el)
          el.sort.join('_')
        end

        def image_path(path, elements)
          { 'path' => format(path, *elements) }
        end

      end

    end
  end
end
