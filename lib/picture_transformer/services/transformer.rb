require 'tinify'
require 'rmagick'
require 'picture_transformer/helpers/log_helpers'

module PictureTransformer
  module Services
    class Transformer

      attr_reader :result

      include LogHelpers

      ::Tinify.key = PictureTransformer.configuration.tinify_key

      def initialize(initial_result)
        @result = initial_result.clone
      end

      def base_path(base)
        @result[:full_path] = File.join(base, @result[:path])
        self
      end

      def load_image
        path = @result[:full_path]
        log("Load image #{path}")
        image = ::Magick::Image.read(path)[0]
        @result[:image] = image
        self
      end

      def resize(width, height)
        log("Resize to #{width}x#{height}")
        @result[:image].resize!(width, height)
        self
      end

      def crops(*crops)
        log "Crop to #{crops}"
        list = ::Magick::ImageList.new
        crops.each { |crop| list << @result[:image].crop(*crop) }
        @result[:images_list] = list
        self
      end

      def destroy_object(key)
        image = @result[key]
        image.destroy!
        @result.delete(key)
        self
      end

      def compress
        @result[:list_filenames].each do |filename|
          tiny_image(filename)
        end
        self
      end

      def quality(value)
        log("Set quality to #{value}%")
        @result[:quality] = value
        self
      end

      def write_image(image_key = :image)
        filename = yield @result
        write_image!(@result[image_key], filename)
        self
      end

      def write_images(list_key = :images_list)
        filenames = []

        @result[list_key].each_with_index do |image, index|
          filename = yield(@result, index)
          write_image!(image, filename)
          filenames << filename
        end

        @result[:list_filenames] = filenames
        self
      end

      def finish!
        result = @result
        remove_instance_variable(:@result)
        result
      end

      private

      def write_image!(image, filename)
        log("Write to #{filename}")
        create_path_to_file(filename)
        quality = @result[:quality] || 100
        image.write(filename) { self.quality = quality }
      end

      def create_path_to_file(filename)
        ::FileUtils.mkdir_p(File.dirname(filename))
      end

      def tiny_image(filename)
        log("Tinify #{filename}")
        original_size = File.size(filename)

        result = ::Tinify.from_file(filename).result
        result.to_file(filename)

        result_size = File.size(filename) # result.size
        saved_percent = ((original_size / result_size.to_f - 1) * 100).round(3)
        saved_kb = ((original_size - result_size) / 1024.0).round(1)

        width = result.width
        height = result.height

        # check how many transformation was done with current API key
        begin
          meta = result.instance_variable_get(:@meta)
          compression_count = meta['Compression-Count']
        rescue Exception
          compression_count = '-'
        end

        log(" Optimized out #{saved_percent}% (#{saved_kb}KB)" \
            "; #{width}x#{height}px" \
            "; Compression-Count: #{compression_count}")
      end

    end
  end
end
