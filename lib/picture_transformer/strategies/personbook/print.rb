require 'picture_transformer/strategies/personbook/base'

module PictureTransformer
  module Strategies
    module Personbook
      class Print < Base

        def call(config, params)
          width, height, crops = params
          transform_service(config)
            .set_logger(-> (*args) { PictureTransformer.logger(*args) })
            .base_path(File.join(PictureTransformer.configuration.base_path, 'personbook'))
            .load_image
            .resize(width, height)
            .crops(*crops)
            .quality(100)
            .write_images { |data, index| filename('print', data, index) }
            .compress # Хак для перевода из cmyk
            .empty_log
            .finish!
        end

      end
    end
  end
end
