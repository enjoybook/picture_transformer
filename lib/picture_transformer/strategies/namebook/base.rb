require 'picture_transformer/services/transformer'
require 'picture_transformer/helpers/measurements'

module PictureTransformer
  module Strategies
    module NameBook
      class Base

        include PictureTransformer::Measurements

        def transform_params(config)
          dps = config['dps']
          width = config['width']
          height = config['height']

          x1, y1, w1, h1 = config['crop1'].map { |x| sm2px(x, dps: dps) }
          x2, y2, w2, h2 = config['crop2'].map { |x| sm2px(x, dps: dps) }

          [
            width, height,
            [
              [x1, y1, w1, h1],
              [x2, y2, w2, h2]
            ]
          ]
        end

        protected

        def transform_service(image_config)
          PictureTransformer::Services::Transformer.new(image_config)
        end

        def filename(prefix_folder, data, index)
          folder = "#{prefix_folder}/#{data[:gender]}"
          name = case data[:type]
          when 'cover', 'hardcover'
            cover_filename data, index
          when 'dedication'
            fixed_filename data, index
          when 'intro'
            fixed_filename data, index
          when 'outro'
            fixed_filename data, index
          when 'filler'
            fixed_filename data, index
          when 'sub'
            sub_filename data, index
          when 'authors'
            fixed_filename data, index
          when 'char'
            char_filename data, index
          else
            raise "Unknown story part type: #{data[:type]}"
          end

          File.join(PictureTransformer.configuration.output_path, 'namebook', folder, name)
        end

        def cover_filename(data, index)
          name = "#{data[:type]}_#{data[:part]}_#{index + 1}.jpg"
          File.join "cover/#{data[:ver]}", name
        end

        def fixed_filename(data, index)
          name = "#{data[:type]}#{data[:part]}_#{index + 1}.jpg"
          File.join "fixed/#{data[:ver]}", name
        end

        def sub_filename(data, index)
          page = data[:part] * 2 + index - 1
          name = if data[:part] == 1
            "1_#{page}.jpg"
          else
            "#{data[:char]}_#{page}.jpg"
          end

          File.join("subs/#{data[:ver]}", name)
        end

        def char_filename(data, index)
          page = data[:part] * 2 + index - 1
          "#{data[:char]}#{data[:ver]}_#{page}.jpg"
        end

      end
    end
  end
end
