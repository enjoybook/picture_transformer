require 'picture_transformer/strategies/herobook/base'

module PictureTransformer
  module Strategies
    module HeroBook
      class Assets < Base

        def call(image, params)
          width, height, crops = params
          transform_service(image)
            .set_logger(-> (*args) { PictureTransformer.logger(*args) })
            .base_path(File.join(PictureTransformer.configuration.base_path, 'herobook'))
            .load_image
            .resize(width, height)
            .crops(*crops)
            .quality(80)
            .write_images { |data, index| filename('assets', data, index) }
            .compress
            .empty_log
            .finish!
        end

      end
    end
  end
end
