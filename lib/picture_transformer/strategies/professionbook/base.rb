require 'picture_transformer/services/transformer'
require 'picture_transformer/helpers/measurements'

module PictureTransformer
  module Strategies
    module Professionbook
      class Base

        include PictureTransformer::Measurements

        def transform_params(config)
          dps = config['dps']
          width = sm2px(config['width'], dps: dps)
          height = sm2px(config['height'], dps: dps)

          crops = config['crops'].map do |crop|
            crop.map { |x| sm2px(x, dps: dps) }
          end

          [width, height, crops]
        end

        protected

        def transform_service(image_config)
          PictureTransformer::Services::Transformer.new(image_config)
        end

        def filename(prefix_folder, data, index)
          base = PictureTransformer.configuration.output_path
          name = File.join(base, 'professionbook', prefix_folder, data[:path])
          name.gsub('.jpg', "_#{index + 1}.jpg")
        end

      end
    end
  end
end
