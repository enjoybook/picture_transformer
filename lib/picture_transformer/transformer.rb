require 'picture_transformer/helpers/memory_helpers'

module PictureTransformer
  class Transformer

    include PictureTransformer::MemoryHelpers

    def call(config, strategy)
      images = config[:images]
      params = strategy.transform_params(config[:transformation])

      images.each_with_index do |image_config, index|
        strategy.call(image_config, params)
        collect_memory if (index % 5).zero?
      end
    end

  end
end
