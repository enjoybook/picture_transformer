module PictureTransformer
  module LogHelpers

    attr_accessor :logger

    def set_logger(logger)
      self.logger = logger
      self
    end

    def print_result
      log(@result)
      self
    end

    def empty_log
      log('')
      self
    end

    private

    def log(*args)
      if logger
        logger.call(*args)
      else
        puts(*args)
      end
    end
  end
end
