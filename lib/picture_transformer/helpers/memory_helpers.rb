module PictureTransformer
  module MemoryHelpers

    def collect_memory
      _pid, size = get_mem
      puts("Memory: #{size}")
      GC.start
    end

    def get_mem
      `ps ax -o pid,rss | grep -E "^[[:space:]]*#{$PID}"`.strip.split.map(&:to_i)
    end

  end
end
