module PictureTransformer
  module Measurements

    INCH = 2.54

    def mm2pt(mm, dpi: 150)
      mm * dpi / INCH
    end

    def sm2px(sm, dps: 150)
      sm * dps
    end
  end
end
