# USAGE

## Checks

Before run transformer for familybook, you should check your images by running the following command:
> CHECK_PATH=./path/to/images rake familybook:checkers:images:original

After slicing you can check consistency of sliced images by:
> CHECK_PATH=./path/to/cutted/images rake familybook:checkers:images:sliced

## Transformer

> TINIFY_KEY=your-tinify-key TRANSFORMER_BASE_PATH=/path/to/sources TRANSFORMER_OUTPUT_PATH=/path/to/output_dir rake familybook:transform:assets


## S4ync

> AWS_KEY_ID=your-aws-key-id AWS_ACCESS_KEY=your-aws-access-key AWS_REGION=your-aws-region UPLOAD_BASE_PATH=/path/to/output_dir rake familybook:s4ync:print


## Test mockup
> tasks/familybook/transform/config/mockup.jpg

The first red line for print cropping.
The second blue line for assets cropping.
The third red line (the smallest quard) for designers: texts should not go out for the borders.

## Command:
> TRANSFORMER_BASE_PATH=./MYNAMEBOOK_SOURCES/source TRANSFORMER_OUTPUT_PATH=./MYNAMEBOOK_SOURCES/output rake familybook:transform:assets

## Make softlink for sources
> ln -s "external path" "local path"

## Copy .env.example and rename to .env
.env in .gitignore. Use it for local configurations


## Picture server 82.202.198.174
