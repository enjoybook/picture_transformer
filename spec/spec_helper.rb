require 'bundler/setup'
require 'picture_transformer'
require 'factory_bot'
require 'byebug'

Dir[
  File.expand_path(File.join(File.dirname(__FILE__),'support','**','*.rb'))
].each { |f| require f }

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    FactoryBot.find_definitions
    PictureTransformer.configure do |c|
      c.base_path = 'spec/fixtures'
      c.output_path = 'spec/output'
      c.tinify_key = 'fake_key'
    end
  end
end

FactoryBot::SyntaxRunner.class_eval do
  include RSpec::Mocks::ExampleMethods
end

