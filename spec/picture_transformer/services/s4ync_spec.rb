require 'spec_helper'

describe PictureTransformer::Services::S4ync do
  subject do
    credentials = { key_id: 'fake_id', access_key: 'fake_key' }
    PictureTransformer::Services::S4ync.new(credentials, 'europe', muted: true)
  end
  let(:local_file) { "#{PictureTransformer.root}/spec/fixtures/share.jpg" }
  let(:bucket) { OpenStruct.new(bucket: 'fake') }

  describe '#sync_images' do
    let(:dir) { "#{PictureTransformer.root}/spec/fixtures" }

    before do
      allow(subject).to receive(:get_bucket).and_return(bucket)
      allow(subject).to receive(:smart_upload).and_return(true)
    end

    it 'calls smart upload file' do
      expect(subject).to receive(:smart_upload).with(bucket, local_file, 'fakedir/share.jpg')

      subject.sync_images(dir, 'fake:fakedir')
    end
  end

  describe '#smart_upload' do
    it 'returns nil unless file passed' do
      result = subject.send(:smart_upload, nil, PictureTransformer.root, nil)

      expect(result).to be_nil
    end

    context 'remote file exist' do
      let(:remote_file) do
        etag = subject.send(:file_etag, local_file)
        OpenStruct.new(etag: etag)
      end

      before { allow(subject).to receive(:get_remote_if_exist).and_return(remote_file) }

      it 'should not upload file' do
        expect(subject).not_to receive(:upload_file)
        subject.send(:smart_upload, bucket, local_file, nil)
      end
    end

    context 'remote file does not exist' do
      before { allow(subject).to receive(:upload_file).and_return(true) }

      it 'uploads file to bucket' do
        expect(subject).to receive(:upload_file)

        subject.send(:smart_upload, bucket, local_file, nil)
      end
    end
  end
end
