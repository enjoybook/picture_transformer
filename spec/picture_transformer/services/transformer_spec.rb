require 'spec_helper'

RSpec.describe PictureTransformer::Services::Transformer do

  let(:image_config) do
    fullpath = File.join('spec', 'fixtures', 'namebook', 'config', 'fixed.yml')
    config = PictureTransformer::Services::ConfigLoader.new.call(fullpath)
    config.map(&:with_indifferent_access)[0]
  end

  subject { PictureTransformer::Services::Transformer.new(image_config) }
  let(:base_path) { PictureTransformer.configuration.base_path }

  describe '#set_logger' do
    it 'stores logger function' do
      subject.set_logger(-> (args) { "#{args}." })
      log_result = subject.send(:log, 'test')

      expect(log_result).to eq('test.')
    end
  end

  describe '#base_path' do
    it 'stores image full path to result' do
      full_path = File.join(base_path, image_config[:path])
      subject.base_path(base_path)

      expect(subject.result[:full_path]).to eq(full_path)
    end
  end

  describe '#load_image' do
    before do
      image_config[:path] = 'share.jpg'
      subject.base_path(base_path)
    end

    it 'stores image to result' do
      subject.load_image

      expect(subject.result[:image]).to be_kind_of(Magick::Image)
    end
  end

  describe '#resize' do
    before do
      image_config[:path] = 'share.jpg'
      subject.base_path(base_path)
      subject.load_image
    end

    it 'resizes image' do
      subject.resize(50, 50)

      expect(subject.result[:image].rows).to eq(50)
      expect(subject.result[:image].columns).to eq(50)
    end
  end

  describe '#write_image' do
    let(:output_path) { 'spec/output/test.jpg' }

    before do
      image_config[:path] = 'share.jpg'
      subject.base_path(base_path)
      subject.load_image
      subject.write_image { |res| output_path }
    end

    it 'resizes image' do
      file = File.open(output_path, 'r')

      expect(file.read).not_to be_empty

      file.close
    end
  end

  describe '#quality' do
    before { subject.quality(60) }

    it 'stores quality to result' do
      expect(subject.result[:quality]).to eq(60)
    end
  end

  describe '#finish!' do
    before do
      image_config[:path] = 'share.jpg'
      subject.base_path(base_path)
      subject.load_image
      subject.finish!
    end

    it 'resets result variable' do
      expect(subject.result).to be_nil
    end
  end

end
