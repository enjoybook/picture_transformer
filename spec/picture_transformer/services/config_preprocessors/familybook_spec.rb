require 'spec_helper'

describe PictureTransformer::Services::ConfigPreprocessor::Familybook do

  subject { described_class }

  describe '#call' do
    context 'when config contains only simple path' do
      it 'returns paths to images' do
        config = [
          {
            'path' => 'intro/grey/find/1_1.jpg'
          }
        ]

        expect(subject.new(config).call).to eq([
          'path' => 'intro/grey/find/1_1.jpg'
        ])
      end
    end

    context 'when config has template path' do
      it 'returns paths to images' do
        config = [
          {
            'path' => 'intro/grey/find/1_%s.jpg',
            'replaces' => [{ 'elements' => [1, 2, 3], 'places' => 1 }]
          }
        ]

        expect(subject.new(config).call).to eq([
          { 'path' => 'intro/grey/find/1_1.jpg' },
          { 'path' => 'intro/grey/find/1_2.jpg' },
          { 'path' => 'intro/grey/find/1_3.jpg' }
        ])
      end
    end

    context 'when there are multiple configs' do
      it 'returns paths to images for each config' do
        config = [
          {
            'path' => 'intro/grey/find/1_%s.jpg',
            'replaces' => [{ 'elements' => [1, 2, 3], 'places' => 1 }]
          },
          {
            'path' => 'intro/brown/find/1_%s.jpg',
            'replaces' => [{ 'elements' => [1, 2, 3], 'places' => 1 }]
          }
        ]

        expect(subject.new(config).call).to eq([
          { 'path' => 'intro/grey/find/1_1.jpg' },
          { 'path' => 'intro/grey/find/1_2.jpg' },
          { 'path' => 'intro/grey/find/1_3.jpg' },
          { 'path' => 'intro/brown/find/1_1.jpg' },
          { 'path' => 'intro/brown/find/1_2.jpg' },
          { 'path' => 'intro/brown/find/1_3.jpg' }
        ])
      end
    end

    context 'when there is a complex template with multiple rules' do
      it 'returns paths to images' do
        config = [
          {
            'path' => 'intro/%s/find/1_%s.jpg',
            'replaces' => [{ 'elements' => ['grey', 'brown'], 'places' => 1 }, { 'elements' => [1, 2, 3], 'places' => 1 }]
          }
        ]

        expect(subject.new(config).call).to eq([
          { 'path' => 'intro/grey/find/1_1.jpg' },
          { 'path' => 'intro/grey/find/1_2.jpg' },
          { 'path' => 'intro/grey/find/1_3.jpg' },
          { 'path' => 'intro/brown/find/1_1.jpg' },
          { 'path' => 'intro/brown/find/1_2.jpg' },
          { 'path' => 'intro/brown/find/1_3.jpg' }
        ])
      end
    end

    context 'when there is complex places in replaces' do
      it 'returns paths to images' do
        config = [
          {
            'path' => 'pulling/2/%s.jpg',
            'replaces' => [{ 'elements' => ['black', 'brown', 'yellow'], 'places' => 2 }]
          }
        ]

        expect(subject.new(config).call).to eq([
          { 'path' => 'pulling/2/black_brown.jpg' },
          { 'path' => 'pulling/2/black_yellow.jpg' },
          { 'path' => 'pulling/2/brown_yellow.jpg' }
        ])
      end
    end

    context 'when there are many replaces' do
      it 'returns paths to images' do
        config = [
          {
            'path' => 'outro/yellow/%s/%s/%s_2.jpg',
            'replaces' => [
              { 'elements' => ['done', 'finish'], 'places' => 1 },
              { 'elements' => [3, 4], 'places' => 1 },
              { 'elements' => ['black', 'brown', 'yellow'], 'places' => 2 }
            ]
          }
        ]

        expect(subject.new(config).call).to eq([
          { 'path' => 'outro/yellow/done/3/black_brown_2.jpg' },
          { 'path' => 'outro/yellow/done/3/black_yellow_2.jpg' },
          { 'path' => 'outro/yellow/done/3/brown_yellow_2.jpg' },
          { 'path' => 'outro/yellow/done/4/black_brown_2.jpg' },
          { 'path' => 'outro/yellow/done/4/black_yellow_2.jpg' },
          { 'path' => 'outro/yellow/done/4/brown_yellow_2.jpg' },
          { 'path' => 'outro/yellow/finish/3/black_brown_2.jpg' },
          { 'path' => 'outro/yellow/finish/3/black_yellow_2.jpg' },
          { 'path' => 'outro/yellow/finish/3/brown_yellow_2.jpg' },
          { 'path' => 'outro/yellow/finish/4/black_brown_2.jpg' },
          { 'path' => 'outro/yellow/finish/4/black_yellow_2.jpg' },
          { 'path' => 'outro/yellow/finish/4/brown_yellow_2.jpg' }
        ])
      end

    end

    context 'when there is keep field in config' do
      it 'returns paths which keep required characters' do
        config = [
          {
            'path' => 'pulling/3/%s.jpg',
            'replaces' => [
              { 'elements' => ['black', 'blue', 'yellow'], 'places' => 2, 'keep' => ['brown'] }
            ]
          }
        ]

        expect(subject.new(config).call).to eq([
          { 'path' => 'pulling/3/black_blue_brown.jpg' },
          { 'path' => 'pulling/3/black_brown_yellow.jpg' },
          { 'path' => 'pulling/3/blue_brown_yellow.jpg' },
        ])
      end
    end

  end
end
