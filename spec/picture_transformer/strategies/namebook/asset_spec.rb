require 'spec_helper'

RSpec.describe PictureTransformer::Strategies::Namebook::Assets do

  let(:image_config) do
    path = File.join('spec', 'fixtures', 'namebook', 'config', 'fixed.yml')
    config = PictureTransformer::Services::ConfigLoader.new.call(path)
    image = config.map(&:with_indifferent_access)[0]
    [image]
  end
  let(:transform_config) do
    path = File.join('spec', 'fixtures', 'namebook', 'config', 'transformation.yml')
    config = PictureTransformer::Services::ConfigLoader.new.call(path)
    config['assets']
  end
  let(:transformation_params) { subject.transform_params(transform_config) }
  let(:transform_service) { PictureTransformer::Services::Transformer.new(image_config[0]) }
  let(:output_path) { PictureTransformer.configuration.output_path }
  subject { PictureTransformer::Strategies::Namebook::Assets.new }

  before do
    allow(transform_service).to receive(:compress).and_return(transform_service)
    allow(subject).to receive(:transform_service).and_return(transform_service)
  end

  it 'writes to result files' do
    subject.call(image_config[0], transformation_params)
    fullpath = File.join(output_path, 'assets', 'girl', 'fixed', 'regular', 'intro1_1.jpg')
    file = File.open(fullpath, 'r')

    expect(file.read).not_to be_empty

    file.close
  end
end
