# frozen_string_literal: true

namespace :namebook do
  namespace :rsync do
    task all: [:assets, :print]

    task :assets do
      from = File.join(namebook_rsync_dir, 'assets')
      namebook_rsync_service.sync_images(from, namebook_rsync_destination, namebook_rsync_server)
    end

    task :print do
      from = File.join(namebook_rsync_dir, 'print')
      namebook_rsync_service.sync_images(from, namebook_rsync_destination, namebook_rsync_server)
    end

    def namebook_rsync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise ArgumentError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'namebook')
    end

    def namebook_rsync_destination
      rsync_destination_path = ENV.fetch('RSYNC_REMOTE_PATH', nil)
      raise ArgumentError, 'You should specify RSYNC_REMOTE_PATH' if rsync_destination_path.nil?
      File.join(rsync_destination_path, 'namebook')
    end

    def namebook_rsync_server
      rsync_destination_server = ENV.fetch('RSYNC_SERVER', nil)
      raise ArgumentError, 'You should specify RSYNC_SERVER' if rsync_destination_server.nil?
      rsync_destination_server
    end

    def namebook_rsync_service
      PictureTransformer::Services::RemoteSync.new
    end
  end
end
