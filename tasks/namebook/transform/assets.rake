# frozen_string_literal: true

namespace :namebook do
  namespace :transform do
    task assets: [:assets_cover, :assets_fixed, :assets_subs, :assets_chars]

    task :assets_cover do
      puts('Start transforming assets_cover')
      config = load_namebook_config('cover.yml', 'assets_cover')
      strategy = PictureTransformer::Strategies::NameBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_fixed do
      puts('Start transforming assets_fixed')
      config = load_namebook_config('fixed.yml', 'assets')
      strategy = PictureTransformer::Strategies::NameBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_subs do
      puts('Start transforming assets_subs')
      config = load_namebook_config('subs.yml', 'assets')
      strategy = PictureTransformer::Strategies::NameBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_chars do
      puts('Start transforming assets_chars')
      config = load_namebook_config('chars.yml', 'assets')
      strategy = PictureTransformer::Strategies::NameBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
