# frozen_string_literal: true

namespace :namebook do
  namespace :transform do
    task print: [:print_cover, :print_hardcover, :print_fixed, :print_subs, :print_chars]

    task :print_fixed do
      puts('Start transforming print_fixed')
      config = load_namebook_config('fixed.yml', 'print')
      strategy = PictureTransformer::Strategies::NameBook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_subs do
      puts('Start transforming print_subs')
      config = load_namebook_config('subs.yml', 'print')
      strategy = PictureTransformer::Strategies::NameBook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_chars do
      puts('Start transforming print_chars')
      config = load_namebook_config('chars.yml', 'print')
      strategy = PictureTransformer::Strategies::NameBook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_cover do
      puts('Start transforming print_cover')
      config = load_namebook_config('cover.yml', 'print_cover')
      strategy = PictureTransformer::Strategies::NameBook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_hardcover do
      puts('Start transforming print_hardcover')
      config = load_namebook_config('hardcover.yml', 'print_hardcover')
      strategy = PictureTransformer::Strategies::NameBook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
