# frozen_string_literal: true

namespace :namebook do
  namespace :transform do
    task all: [:assets, :print]
    task all_fixed: [:assets_fixed, :print_fixed]
    task all_chars: [:assets_chars, :print_chars]
    task all_subs: [:assets_subs, :print_subs]

    def namebook_images_config(path)
      fullpath = File.join(File.dirname(__FILE__), 'config', path)
      config_loader = PictureTransformer::Services::ConfigLoader.new
      config = config_loader.call(fullpath)
      config_loader.extend_config_with_chars(config)
    end

    def namebook_transformation_config(transformation)
      fullpath = File.join(File.dirname(__FILE__), 'config', 'transformation.yml')
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)[transformation]
    end

    def load_namebook_config(path, transformation)
      {
        images: namebook_images_config(path).map(&:with_indifferent_access),
        transformation: namebook_transformation_config(transformation)
      }
    end
  end
end


