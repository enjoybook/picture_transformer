# frozen_string_literal: true

namespace :namebook do
  namespace :s4ync do
    task all: [:assets, :print]

    task :assets do
      namebook_s4ync_service.sync_images(File.join(namebook_s4ync_dir, 'assets'), 'mynamebook-assets:book')
    end

    task :print do
      namebook_s4ync_service.sync_images(File.join(namebook_s4ync_dir, 'print'), 'mynamebook:cherrypie')
    end

    def namebook_s4ync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise StandardError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'namebook')
    end

    def namebook_s4ync_service
      credentials = {
        key_id: ENV.fetch('AWS_KEY_ID', nil),
        access_key: ENV.fetch('AWS_ACCESS_KEY', nil)
      }
      PictureTransformer::Services::S4ync.new(credentials, ENV.fetch('AWS_REGION', nil))
    end
  end
end
