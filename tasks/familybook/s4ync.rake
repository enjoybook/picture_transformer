# frozen_string_literal: true

namespace :familybook do
  namespace :s4ync do

    task :print do
      familybook_s4ync_service.sync_images(File.join(familybook_s4ync_dir, 'print'), 'mynamebook:family_book/print')
    end

    def familybook_s4ync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise StandardError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'familybook')
    end

    def familybook_s4ync_service
      credentials = {
        key_id: ENV.fetch('AWS_KEY_ID', nil),
        access_key: ENV.fetch('AWS_ACCESS_KEY', nil)
      }

      PictureTransformer::Services::S4ync.new(credentials, ENV.fetch('AWS_REGION', nil))
    end
  end
end
