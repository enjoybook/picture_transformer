# frozen_string_literal: true

namespace :familybook do
  namespace :checkers do
    namespace :images do
      task :original do
        images = PictureTransformer::Services::ConfigPreprocessor::Familybook.new(load_config('sheets.yml')).call
        PictureTransformer::Services::Checkers::Familybook.new(ENV['CHECK_PATH'], images).call
      end

      task :sliced do
        images = PictureTransformer::Services::ConfigPreprocessor::Familybook.new(load_config('sheets.yml')).call
        PictureTransformer::Services::Checkers::Familybook.new(ENV['CHECK_PATH'], images, { check_sliced: true }).call
      end
    end
  end

  namespace :transform do
    def familybook_images_config(path)
      PictureTransformer::Services::ConfigPreprocessor::Familybook.new(load_config(path)).call
    end

    def familybook_transformation_config(transformation)
      fullpath = File.join(File.dirname(__FILE__), 'config', 'transformation.yml')
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)[transformation]
    end

    def load_familybook_config(path, transformation)
      {
        images: familybook_images_config(path).map(&:with_indifferent_access),
        transformation: familybook_transformation_config(transformation)
      }
    end
  end
end

def load_config(path)
  fullpath = File.join(File.dirname(__FILE__), 'config', path)
  PictureTransformer::Services::ConfigLoader.new.call(fullpath)
end
