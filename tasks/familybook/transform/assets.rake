# frozen_string_literal: true

namespace :familybook do
  namespace :transform do
    task assets: [:assets_cover, :assets_pictures]

    task :assets_pictures do
      puts('Start transforming assets')
      config = load_familybook_config('sheets.yml', 'assets')
      strategy = PictureTransformer::Strategies::FamilyBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_cover do
      puts('Start transforming assets_cover')
      config = load_familybook_config('cover.yml', 'assets_cover')
      strategy = PictureTransformer::Strategies::FamilyBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
