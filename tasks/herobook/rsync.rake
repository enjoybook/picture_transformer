# frozen_string_literal: true

namespace :herobook do
  namespace :rsync do
    task all: [:assets, :print]

    task :assets do
      herobook_rsync_service.sync_images(herobook_rsync_source_folder('assets'), herobook_rsync_destination, herobook_rsync_server)
    end

    task :print do
      herobook_rsync_service.sync_images(herobook_rsync_source_folder('print'), herobook_rsync_destination, herobook_rsync_server)
    end

    def herobook_rsync_source_folder(path)
      File.join(herobook_rsync_dir, path)
    end

    def herobook_rsync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise ArgumentError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'herobook')
    end

    def herobook_rsync_destination
      rsync_destination_path = ENV.fetch('RSYNC_REMOTE_PATH', nil)
      raise ArgumentError, 'You should specify RSYNC_REMOTE_PATH' if rsync_destination_path.nil?
      File.join(rsync_destination_path, 'herobook')
    end

    def herobook_rsync_server
      rsync_destination_server = ENV.fetch('RSYNC_SERVER', nil)
      raise ArgumentError, 'You should specify RSYNC_SERVER' if rsync_destination_server.nil?
      rsync_destination_server
    end

    def herobook_rsync_service
      PictureTransformer::Services::RemoteSync.new
    end
  end
end
