# frozen_string_literal: true

namespace :herobook do
  namespace :transform do
    task assets: [:assets_cover, :assets_pictures]

    task :assets_pictures do
      puts('Start transforming assets_pictures')
      config = load_herobook_config('pictures.yml', 'assets')
      strategy = PictureTransformer::Strategies::HeroBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_cover do
      puts('Start transforming assets_cover')
      config = load_herobook_config('cover.yml', 'assets_cover')
      strategy = PictureTransformer::Strategies::HeroBook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
