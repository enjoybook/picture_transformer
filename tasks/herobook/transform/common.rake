# frozen_string_literal: true

namespace :herobook do
  namespace :transform do
    task all: [:assets, :print]

    def herobook_images_config(path)
      fullpath = File.join(File.dirname(__FILE__), 'config', path)
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)
    end

    def herobook_transformation_config(transformation)
      fullpath = File.join(File.dirname(__FILE__), 'config', 'transformation.yml')
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)[transformation]
    end

    def load_herobook_config(path, transformation)
      {
        images: herobook_images_config(path).map(&:with_indifferent_access),
        transformation: herobook_transformation_config(transformation)
      }
    end
  end
end
