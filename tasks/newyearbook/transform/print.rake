# frozen_string_literal: true

namespace :newyearbook do
  namespace :transform do
    task print: [:print_cover, :print_hardcover, :print_pictures]

    task :print_pictures do
      puts('Start transforming print')
      config = load_newyearbook_config('pictures.yml', 'print')
      strategy = PictureTransformer::Strategies::Newyearbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_cover do
      puts('Start transforming print_cover')
      config = load_newyearbook_config('cover.yml', 'print_cover')
      strategy = PictureTransformer::Strategies::Newyearbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_hardcover do
      puts('Start transforming print_hardcover')
      config = load_newyearbook_config('hardcover.yml', 'print_hardcover')
      strategy = PictureTransformer::Strategies::Newyearbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
