# frozen_string_literal: true

namespace :newyearbook do
  namespace :transform do
    task all: [:assets, :print]

    def newyearbook_images_config(path)
      fullpath = File.join(File.dirname(__FILE__), 'config', path)
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)
    end

    def newyearbook_transformation_config(transformation)
      fullpath = File.join(File.dirname(__FILE__), 'config', 'transformation.yml')
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)[transformation]
    end

    def load_newyearbook_config(path, transformation)
      {
        images: newyearbook_images_config(path).map(&:with_indifferent_access),
        transformation: newyearbook_transformation_config(transformation)
      }
    end
  end
end
