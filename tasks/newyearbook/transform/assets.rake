# frozen_string_literal: true

namespace :newyearbook do
  namespace :transform do
    task assets: [:assets_cover, :assets_pictures]

    task :assets_pictures do
      puts('Start transforming assets_pictures')
      config = load_newyearbook_config('pictures.yml', 'assets')
      strategy = PictureTransformer::Strategies::Newyearbook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_cover do
      puts('Start transforming assets_cover')
      config = load_newyearbook_config('cover.yml', 'assets_cover')
      strategy = PictureTransformer::Strategies::Newyearbook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
