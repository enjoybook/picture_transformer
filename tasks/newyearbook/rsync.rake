# frozen_string_literal: true

namespace :newyearbook do
  namespace :rsync do
    task all: [:assets, :print]

    task :assets do
      newyearbook_rsync_service.sync_images(newyearbook_rsync_source_folder('assets'), newyearbook_rsync_destination, newyearbook_rsync_server)
    end

    task :print do
      newyearbook_rsync_service.sync_images(newyearbook_rsync_source_folder('print'), newyearbook_rsync_destination, newyearbook_rsync_server)
    end

    def newyearbook_rsync_source_folder(path)
      File.join(newyearbook_rsync_dir, path)
    end

    def newyearbook_rsync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise ArgumentError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'newyearbook')
    end

    def newyearbook_rsync_destination
      rsync_destination_path = ENV.fetch('RSYNC_REMOTE_PATH', nil)
      raise ArgumentError, 'You should specify RSYNC_REMOTE_PATH' if rsync_destination_path.nil?
      File.join(rsync_destination_path, 'newyearbook')
    end

    def newyearbook_rsync_server
      rsync_destination_server = ENV.fetch('RSYNC_SERVER', nil)
      raise ArgumentError, 'You should specify RSYNC_SERVER' if rsync_destination_server.nil?
      rsync_destination_server
    end

    def newyearbook_rsync_service
      PictureTransformer::Services::RemoteSync.new
    end
  end
end
