# frozen_string_literal: true

namespace :personbook do
  namespace :rsync do
    task all: [:assets, :print]

    task :assets do
      personbook_rsync_service.sync_images(personbook_rsync_source_folder('assets'), personbook_rsync_destination, personbook_rsync_server)
    end

    task :print do
      personbook_rsync_service.sync_images(personbook_rsync_source_folder('print'), personbook_rsync_destination, personbook_rsync_server)
    end

    def personbook_rsync_source_folder(path)
      File.join(personbook_rsync_dir, path)
    end

    def personbook_rsync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise ArgumentError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'personbook')
    end

    def personbook_rsync_destination
      rsync_destination_path = ENV.fetch('RSYNC_REMOTE_PATH', nil)
      raise ArgumentError, 'You should specify RSYNC_REMOTE_PATH' if rsync_destination_path.nil?
      File.join(rsync_destination_path, 'personbook')
    end

    def personbook_rsync_server
      rsync_destination_server = ENV.fetch('RSYNC_SERVER', nil)
      raise ArgumentError, 'You should specify RSYNC_SERVER' if rsync_destination_server.nil?
      rsync_destination_server
    end

    def personbook_rsync_service
      PictureTransformer::Services::RemoteSync.new
    end
  end
end
