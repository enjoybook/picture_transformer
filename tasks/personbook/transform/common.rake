# frozen_string_literal: true

namespace :personbook do
  namespace :transform do
    task all: [:assets, :print]

    def personbook_images_config(path)
      fullpath = File.join(File.dirname(__FILE__), 'config', path)
      config = PictureTransformer::Services::ConfigLoader.new.call(fullpath)

      PictureTransformer::Services::ConfigPreprocessor::Familybook.new(config).call
    end

    def personbook_transformation_config(transformation)
      fullpath = File.join(File.dirname(__FILE__), 'config', 'transformation.yml')
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)[transformation]
    end

    def load_personbook_config(path, transformation)
      {
        images: personbook_images_config(path).map(&:with_indifferent_access),
        transformation: personbook_transformation_config(transformation)
      }
    end
  end
end
