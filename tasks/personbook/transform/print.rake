# frozen_string_literal: true

namespace :personbook do
  namespace :transform do
    task print: [:print_cover, :print_hardcover, :print_pictures]

    task :print_pictures do
      puts('Start transforming print')
      config = load_personbook_config('pictures.yml', 'print')
      strategy = PictureTransformer::Strategies::Personbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_cover do
      puts('Start transforming print_cover')
      config = load_personbook_config('cover.yml', 'print_cover')
      strategy = PictureTransformer::Strategies::Personbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_hardcover do
      puts('Start transforming print_hardcover')
      config = load_personbook_config('hardcover.yml', 'print_hardcover')
      strategy = PictureTransformer::Strategies::Personbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
