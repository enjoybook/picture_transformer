# frozen_string_literal: true

namespace :professionbook do
  namespace :rsync do
    task all: [:assets, :print]

    task :assets do
      professionbook_rsync_service.sync_images(professionbook_rsync_source_folder('assets'), professionbook_rsync_destination, professionbook_rsync_server)
    end

    task :print do
      professionbook_rsync_service.sync_images(professionbook_rsync_source_folder('print'), professionbook_rsync_destination, professionbook_rsync_server)
    end

    def professionbook_rsync_source_folder(path)
      File.join(professionbook_rsync_dir, path)
    end

    def professionbook_rsync_dir
      upload_base_path = ENV.fetch('UPLOAD_BASE_PATH', nil)
      raise ArgumentError, 'You should specify upload base path' if upload_base_path.nil?
      File.join(upload_base_path, 'professionbook')
    end

    def professionbook_rsync_destination
      rsync_destination_path = ENV.fetch('RSYNC_REMOTE_PATH', nil)
      raise ArgumentError, 'You should specify RSYNC_REMOTE_PATH' if rsync_destination_path.nil?
      File.join(rsync_destination_path, 'professionbook')
    end

    def professionbook_rsync_server
      rsync_destination_server = ENV.fetch('RSYNC_SERVER', nil)
      raise ArgumentError, 'You should specify RSYNC_SERVER' if rsync_destination_server.nil?
      rsync_destination_server
    end

    def professionbook_rsync_service
      PictureTransformer::Services::RemoteSync.new
    end
  end
end
