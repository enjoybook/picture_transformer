# frozen_string_literal: true

namespace :professionbook do
  namespace :transform do
    task all: [:assets, :print]

    def professionbook_images_config(path)
      fullpath = File.join(File.dirname(__FILE__), 'config', path)
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)
    end

    def professionbook_transformation_config(transformation)
      fullpath = File.join(File.dirname(__FILE__), 'config', 'transformation.yml')
      PictureTransformer::Services::ConfigLoader.new.call(fullpath)[transformation]
    end

    def load_professionbook_config(path, transformation)
      {
        images: professionbook_images_config(path).map(&:with_indifferent_access),
        transformation: professionbook_transformation_config(transformation)
      }
    end
  end
end
