# frozen_string_literal: true

namespace :professionbook do
  namespace :transform do
    task print: [:print_cover, :print_hardcover, :print_pictures]

    task :print_pictures do
      puts('Start transforming print_pictures')
      config = load_professionbook_config('pictures.yml', 'print')
      strategy = PictureTransformer::Strategies::Professionbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_cover do
      puts('Start transforming print_cover')
      config = load_professionbook_config('cover.yml', 'print_cover')
      strategy = PictureTransformer::Strategies::Professionbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :print_hardcover do
      puts('Start transforming print_hardcover')
      config = load_professionbook_config('hardcover.yml', 'print_hardcover')
      strategy = PictureTransformer::Strategies::Professionbook::Print.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
