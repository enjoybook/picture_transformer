# frozen_string_literal: true

namespace :professionbook do
  namespace :transform do
    task assets: [:assets_cover, :assets_pictures]

    task :assets_pictures do
      puts('Start transforming assets_pictures')
      config = load_professionbook_config('pictures.yml', 'assets')
      strategy = PictureTransformer::Strategies::Professionbook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end

    task :assets_cover do
      puts('Start transforming assets_cover')
      config = load_professionbook_config('cover.yml', 'assets_cover')
      strategy = PictureTransformer::Strategies::Professionbook::Assets.new
      PictureTransformer::Transformer.new.call(config, strategy)
    end
  end
end
